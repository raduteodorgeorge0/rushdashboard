import React from "react";

export function Candidates() {
    return (
    <>
        <div className="content d-flex flex-column flex-column-fluid" id="kt_content">
          <div className="subheader py-2 py-lg-4 subheader-transparent" id="kt_subheader">
            <div className="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
              <div className="d-flex align-items-center flex-wrap mr-2">
                <h5 className="text-dark font-weight-bold mt-2 mb-2 mr-5">Users</h5>
                <div className="subheader-separator subheader-separator-ver mt-2 mb-2 mr-5 bg-gray-200" />
                <div className="d-flex align-items-center" id="kt_subheader_search">
                  <span className="text-dark-50 font-weight-bold" id="kt_subheader_total">450 Total</span>
                  <form className="ml-5">
                    <div className="input-group input-group-sm input-group-solid" style={{maxWidth: '175px'}}>
                      <input type="text" className="form-control" id="kt_subheader_search_form" placeholder="Search..." />
                      <div className="input-group-append">
                        <span className="input-group-text">
                          <span className="svg-icon">
                            <svg xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                              <g stroke="none" strokeWidth={1} fill="none" fillRule="evenodd">
                                <rect x={0} y={0} width={24} height={24} />
                                <path d="M14.2928932,16.7071068 C13.9023689,16.3165825 13.9023689,15.6834175 14.2928932,15.2928932 C14.6834175,14.9023689 15.3165825,14.9023689 15.7071068,15.2928932 L19.7071068,19.2928932 C20.0976311,19.6834175 20.0976311,20.3165825 19.7071068,20.7071068 C19.3165825,21.0976311 18.6834175,21.0976311 18.2928932,20.7071068 L14.2928932,16.7071068 Z" fill="#000000" fillRule="nonzero" opacity="0.3" />
                                <path d="M11,16 C13.7614237,16 16,13.7614237 16,11 C16,8.23857625 13.7614237,6 11,6 C8.23857625,6 6,8.23857625 6,11 C6,13.7614237 8.23857625,16 11,16 Z M11,18 C7.13400675,18 4,14.8659932 4,11 C4,7.13400675 7.13400675,4 11,4 C14.8659932,4 18,7.13400675 18,11 C18,14.8659932 14.8659932,18 11,18 Z" fill="#000000" fillRule="nonzero" />
                              </g>
                            </svg>
                          </span>
                        </span>
                      </div>
                    </div>
                  </form>
                </div>
                <div className="d-flex- align-items-center flex-wrap mr-2 d-none" id="kt_subheader_group_actions">
                  <div className="text-dark-50 font-weight-bold">
                    <span id="kt_subheader_group_selected_rows">23</span>Selected:</div>
                  <div className="d-flex ml-6">
                    <div className="dropdown mr-2" id="kt_subheader_group_actions_status_change">
                      <button type="button" className="btn btn-light-primary font-weight-bolder btn-sm dropdown-toggle" data-toggle="dropdown">Update Status</button>
                      <div className="dropdown-menu p-0 m-0 dropdown-menu-sm">
                        <ul className="navi navi-hover pt-3 pb-4">
                          <li className="navi-header font-weight-bolder text-uppercase text-primary font-size-lg pb-0">Change status to:</li>
                          <li className="navi-item">
                            <a href="#" className="navi-link" data-toggle="status-change" data-status={1}>
                              <span className="navi-text">
                                <span className="label label-light-success label-inline font-weight-bold">Approved</span>
                              </span>
                            </a>
                          </li>
                          <li className="navi-item">
                            <a href="#" className="navi-link" data-toggle="status-change" data-status={2}>
                              <span className="navi-text">
                                <span className="label label-light-danger label-inline font-weight-bold">Rejected</span>
                              </span>
                            </a>
                          </li>
                          <li className="navi-item">
                            <a href="#" className="navi-link" data-toggle="status-change" data-status={3}>
                              <span className="navi-text">
                                <span className="label label-light-warning label-inline font-weight-bold">Pending</span>
                              </span>
                            </a>
                          </li>
                          <li className="navi-item">
                            <a href="#" className="navi-link" data-toggle="status-change" data-status={4}>
                              <span className="navi-text">
                                <span className="label label-light-info label-inline font-weight-bold">On Hold</span>
                              </span>
                            </a>
                          </li>
                        </ul>
                      </div>
                    </div>
                    <button className="btn btn-light-success font-weight-bolder btn-sm mr-2" id="kt_subheader_group_actions_fetch" data-toggle="modal" data-target="#kt_datatable_records_fetch_modal">Fetch Selected</button>
                    <button className="btn btn-light-danger font-weight-bolder btn-sm mr-2" id="kt_subheader_group_actions_delete_all">Delete All</button>
                  </div>
                </div>
              </div>
              <div className="d-flex align-items-center">
                <a href="#" className />
                <a href="custom/apps/user/add-user.html" className="btn btn-light-primary font-weight-bold ml-2">Add User</a>
                <div className="dropdown dropdown-inline ml-2" data-toggle="tooltip" title="Quick actions" data-placement="left">
                  <a href="#" className="btn btn-icon" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <span className="svg-icon svg-icon-success svg-icon-2x">
                      <svg xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                        <g stroke="none" strokeWidth={1} fill="none" fillRule="evenodd">
                          <polygon points="0 0 24 0 24 24 0 24" />
                          <path d="M5.85714286,2 L13.7364114,2 C14.0910962,2 14.4343066,2.12568431 14.7051108,2.35473959 L19.4686994,6.3839416 C19.8056532,6.66894833 20,7.08787823 20,7.52920201 L20,20.0833333 C20,21.8738751 19.9795521,22 18.1428571,22 L5.85714286,22 C4.02044787,22 4,21.8738751 4,20.0833333 L4,3.91666667 C4,2.12612489 4.02044787,2 5.85714286,2 Z" fill="#000000" fillRule="nonzero" opacity="0.3" />
                          <path d="M11,14 L9,14 C8.44771525,14 8,13.5522847 8,13 C8,12.4477153 8.44771525,12 9,12 L11,12 L11,10 C11,9.44771525 11.4477153,9 12,9 C12.5522847,9 13,9.44771525 13,10 L13,12 L15,12 C15.5522847,12 16,12.4477153 16,13 C16,13.5522847 15.5522847,14 15,14 L13,14 L13,16 C13,16.5522847 12.5522847,17 12,17 C11.4477153,17 11,16.5522847 11,16 L11,14 Z" fill="#000000" />
                        </g>
                      </svg>
                    </span>
                  </a>
                  <div className="dropdown-menu p-0 m-0 dropdown-menu-md dropdown-menu-right">
                    <ul className="navi">
                      <li className="navi-header font-weight-bold py-5">
                        <span className="font-size-lg">Add New:</span>
                        <i className="flaticon2-information icon-md text-muted" data-toggle="tooltip" data-placement="right" title="Click to learn more..." />
                      </li>
                      <li className="navi-separator mb-3 opacity-70" />
                      <li className="navi-item">
                        <a href="#" className="navi-link">
                          <span className="navi-icon">
                            <i className="flaticon2-shopping-cart-1" />
                          </span>
                          <span className="navi-text">Order</span>
                        </a>
                      </li>
                      <li className="navi-item">
                        <a href="#" className="navi-link">
                          <span className="navi-icon">
                            <i className="navi-icon flaticon2-calendar-8" />
                          </span>
                          <span className="navi-text">Members</span>
                          <span className="navi-label">
                            <span className="label label-light-danger label-rounded font-weight-bold">3</span>
                          </span>
                        </a>
                      </li>
                      <li className="navi-item">
                        <a href="#" className="navi-link">
                          <span className="navi-icon">
                            <i className="navi-icon flaticon2-telegram-logo" />
                          </span>
                          <span className="navi-text">Project</span>
                        </a>
                      </li>
                      <li className="navi-item">
                        <a href="#" className="navi-link">
                          <span className="navi-icon">
                            <i className="navi-icon flaticon2-new-email" />
                          </span>
                          <span className="navi-text">Record</span>
                          <span className="navi-label">
                            <span className="label label-light-success label-rounded font-weight-bold">5</span>
                          </span>
                        </a>
                      </li>
                      <li className="navi-separator mt-3 opacity-70" />
                      <li className="navi-footer pt-5 pb-4">
                        <a className="btn btn-light-primary font-weight-bolder btn-sm" href="#">More options</a>
                        <a className="btn btn-clean font-weight-bold btn-sm d-none" href="#" data-toggle="tooltip" data-placement="right" title="Click to learn more...">Learn more</a>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="d-flex flex-column-fluid">
            <div className="container">
              <div className="card card-custom gutter-b">
                <div className="card-body">
                  <div className="d-flex">
                    <div className="flex-shrink-0 mr-7">
                    <div className="symbol symbol-50 symbol-lg-120 symbol-light-danger">
                        <span className="font-size-h3 symbol-label font-weight-boldest">GM</span>
                      </div>
                    </div>
                    <div className="flex-grow-1">
                      <div className="d-flex align-items-center justify-content-between flex-wrap mt-2">
                        <div className="mr-3">
                          <a href="#" className="d-flex align-items-center text-dark text-hover-primary font-size-h5 font-weight-bold mr-3">George Marian
                            <i className="flaticon2-correct text-success icon-md ml-2" /></a>
                          <div className="d-flex flex-wrap my-2">
                            <a href="#" className="text-muted text-hover-primary font-weight-bold mr-lg-8 mr-5 mb-lg-0 mb-2">
                              <span className="svg-icon svg-icon-md svg-icon-gray-500 mr-1">
                                <svg xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                  <g stroke="none" strokeWidth={1} fill="none" fillRule="evenodd">
                                    <rect x={0} y={0} width={24} height={24} />
                                    <path d="M21,12.0829584 C20.6747915,12.0283988 20.3407122,12 20,12 C16.6862915,12 14,14.6862915 14,18 C14,18.3407122 14.0283988,18.6747915 14.0829584,19 L5,19 C3.8954305,19 3,18.1045695 3,17 L3,8 C3,6.8954305 3.8954305,6 5,6 L19,6 C20.1045695,6 21,6.8954305 21,8 L21,12.0829584 Z M18.1444251,7.83964668 L12,11.1481833 L5.85557487,7.83964668 C5.4908718,7.6432681 5.03602525,7.77972206 4.83964668,8.14442513 C4.6432681,8.5091282 4.77972206,8.96397475 5.14442513,9.16035332 L11.6444251,12.6603533 C11.8664074,12.7798822 12.1335926,12.7798822 12.3555749,12.6603533 L18.8555749,9.16035332 C19.2202779,8.96397475 19.3567319,8.5091282 19.1603533,8.14442513 C18.9639747,7.77972206 18.5091282,7.6432681 18.1444251,7.83964668 Z" fill="#000000" />
                                    <circle fill="#000000" opacity="0.3" cx="19.5" cy="17.5" r="2.5" />
                                  </g>
                                </svg>
                              </span>george.marian@example.com</a>
                            <a href="#" className="text-muted text-hover-primary font-weight-bold mr-lg-8 mr-5 mb-lg-0 mb-2">
                              <span className="svg-icon svg-icon-md svg-icon-gray-500 mr-1">
                                <svg xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                  <g stroke="none" strokeWidth={1} fill="none" fillRule="evenodd">
                                    <mask fill="white">
                                      <use xlinkHref="#path-1" />
                                    </mask>
                                    <g />
                                    <path d="M7,10 L7,8 C7,5.23857625 9.23857625,3 12,3 C14.7614237,3 17,5.23857625 17,8 L17,10 L18,10 C19.1045695,10 20,10.8954305 20,12 L20,18 C20,19.1045695 19.1045695,20 18,20 L6,20 C4.8954305,20 4,19.1045695 4,18 L4,12 C4,10.8954305 4.8954305,10 6,10 L7,10 Z M12,5 C10.3431458,5 9,6.34314575 9,8 L9,10 L15,10 L15,8 C15,6.34314575 13.6568542,5 12,5 Z" fill="#000000" />
                                  </g>
                                </svg>
                              </span>PR Manager</a>
                            <a href="#" className="text-muted text-hover-primary font-weight-bold">
                              <span className="svg-icon svg-icon-md svg-icon-gray-500 mr-1">
                                <svg xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                  <g stroke="none" strokeWidth={1} fill="none" fillRule="evenodd">
                                    <rect x={0} y={0} width={24} height={24} />
                                    <path d="M9.82829464,16.6565893 C7.02541569,15.7427556 5,13.1079084 5,10 C5,6.13400675 8.13400675,3 12,3 C15.8659932,3 19,6.13400675 19,10 C19,13.1079084 16.9745843,15.7427556 14.1717054,16.6565893 L12,21 L9.82829464,16.6565893 Z M12,12 C13.1045695,12 14,11.1045695 14,10 C14,8.8954305 13.1045695,8 12,8 C10.8954305,8 10,8.8954305 10,10 C10,11.1045695 10.8954305,12 12,12 Z" fill="#000000" />
                                  </g>
                                </svg>
                              </span>Bucharest</a>
                          </div>
                        </div>
                        <div className="my-lg-0 my-1">
                          <a href="#" className="btn btn-sm btn-light-primary font-weight-bolder text-uppercase mr-2">Ask</a>
                          <a href="#" className="btn btn-sm btn-primary font-weight-bolder text-uppercase">Hire</a>
                        </div>
                      </div>
                      <div className="d-flex align-items-center flex-wrap justify-content-between">
                        <div className="flex-grow-1 font-weight-bold text-dark-50 py-2 py-lg-2 mr-5">Experimentat in domeniu cu 10+ ani in PR si Marketing <br/> GMA si creativitate sporita . Match ridicat in cultura companiei.  </div>
                        <div className="d-flex mt-4 mt-sm-0">
                          <span className="font-weight-bold mr-4">Match</span>
                          <div className="progress progress-xs mt-2 mb-2 flex-shrink-0 w-150px w-xl-250px">
                            <div className="progress-bar bg-success" role="progressbar" style={{width: '97%'}} aria-valuenow={50} aria-valuemin={0} aria-valuemax={100} />
                          </div>
                          <span className="font-weight-bolder text-dark ml-4">97%</span>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="separator separator-solid my-7" />
                  <div className="d-flex align-items-center flex-wrap">
                    <div className="d-flex align-items-center flex-lg-fill mr-5 my-1">
                      <span className="mr-4">
                        <i className="flaticon-piggy-bank icon-2x text-muted font-weight-bold" />
                      </span>
                      <div className="d-flex flex-column text-dark-75">
                        <span className="font-weight-bolder font-size-sm">Raw Value</span>
                        <span className="font-weight-bolder font-size-h5">
                          <span className="text-dark-50 font-weight-bold">$</span>1200</span>
                      </div>
                    </div>
                    <div className="d-flex align-items-center flex-lg-fill mr-5 my-1">
                      <span className="mr-4">
                        <i className="flaticon-confetti icon-2x text-muted font-weight-bold" />
                      </span>
                      <div className="d-flex flex-column text-dark-75">
                        <span className="font-weight-bolder font-size-sm">GMA score</span>
                        <span className="font-weight-bolder font-size-h5">
                          <span className="text-dark-50 font-weight-bold">$</span>98,000</span>
                      </div>
                    </div>
                    <div className="d-flex align-items-center flex-lg-fill mr-5 my-1">
                      <span className="mr-4">
                        <i className="flaticon-pie-chart icon-2x text-muted font-weight-bold" />
                      </span>
                      <div className="d-flex flex-column text-dark-75">
                        <span className="font-weight-bolder font-size-sm">Culture match</span>
                        <span className="font-weight-bolder font-size-h5">
                          <span className="text-dark-50 font-weight-bold">$</span>94%</span>
                      </div>
                    </div>
                    <div className="d-flex align-items-center flex-lg-fill mr-5 my-1">
                      <span className="mr-4">
                        <i className="flaticon-file-2 icon-2x text-muted font-weight-bold" />
                      </span>
                      <div className="d-flex flex-column flex-lg-fill">
                        <span className="text-dark-75 font-weight-bolder font-size-sm">7 Jobs</span>
                        <a href="#" className="text-primary font-weight-bolder">View</a>
                      </div>
                    </div>
                    <div className="d-flex align-items-center flex-lg-fill mr-5 my-1">
                      <span className="mr-4">
                        <i className="flaticon-chat-1 icon-2x text-muted font-weight-bold" />
                      </span>
                      <div className="d-flex flex-column">
                        <span className="text-dark-75 font-weight-bolder font-size-sm">2 Recommandation</span>
                        <a href="#" className="text-primary font-weight-bolder">View</a>
                      </div>
                    </div>
                    
                  </div>
                </div>
              </div>
              <div className="card card-custom gutter-b">
                <div className="card-body">
                  <div className="d-flex">
                    <div className="flex-shrink-0 mr-7">
                      <div className="symbol symbol-50 symbol-lg-120 symbol-light-danger">
                        <span className="font-size-h3 symbol-label font-weight-boldest">MP</span>
                      </div>
                    </div>
                    <div className="flex-grow-1">
                      <div className="d-flex align-items-center justify-content-between flex-wrap mt-2">
                        <div className="mr-3">
                          <a href="#" className="d-flex align-items-center text-dark text-hover-primary font-size-h5 font-weight-bold mr-3">Andreea Danescu
                            <i className="flaticon2-correct text-success icon-md ml-2" /></a>
                          <div className="d-flex flex-wrap my-2">
                            <a href="#" className="text-muted text-hover-primary font-weight-bold mr-lg-8 mr-5 mb-lg-0 mb-2">
                              <span className="svg-icon svg-icon-md svg-icon-gray-500 mr-1">
                                <svg xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                  <g stroke="none" strokeWidth={1} fill="none" fillRule="evenodd">
                                    <rect x={0} y={0} width={24} height={24} />
                                    <path d="M21,12.0829584 C20.6747915,12.0283988 20.3407122,12 20,12 C16.6862915,12 14,14.6862915 14,18 C14,18.3407122 14.0283988,18.6747915 14.0829584,19 L5,19 C3.8954305,19 3,18.1045695 3,17 L3,8 C3,6.8954305 3.8954305,6 5,6 L19,6 C20.1045695,6 21,6.8954305 21,8 L21,12.0829584 Z M18.1444251,7.83964668 L12,11.1481833 L5.85557487,7.83964668 C5.4908718,7.6432681 5.03602525,7.77972206 4.83964668,8.14442513 C4.6432681,8.5091282 4.77972206,8.96397475 5.14442513,9.16035332 L11.6444251,12.6603533 C11.8664074,12.7798822 12.1335926,12.7798822 12.3555749,12.6603533 L18.8555749,9.16035332 C19.2202779,8.96397475 19.3567319,8.5091282 19.1603533,8.14442513 C18.9639747,7.77972206 18.5091282,7.6432681 18.1444251,7.83964668 Z" fill="#000000" />
                                    <circle fill="#000000" opacity="0.3" cx="19.5" cy="17.5" r="2.5" />
                                  </g>
                                </svg>
                              </span>andreea.danescu@example.com</a>
                            <a href="#" className="text-muted text-hover-primary font-weight-bold mr-lg-8 mr-5 mb-lg-0 mb-2">
                              <span className="svg-icon svg-icon-md svg-icon-gray-500 mr-1">
                                <svg xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                  <g stroke="none" strokeWidth={1} fill="none" fillRule="evenodd">
                                    <mask fill="white">
                                      <use xlinkHref="#path-1" />
                                    </mask>
                                    <g />
                                    <path d="M7,10 L7,8 C7,5.23857625 9.23857625,3 12,3 C14.7614237,3 17,5.23857625 17,8 L17,10 L18,10 C19.1045695,10 20,10.8954305 20,12 L20,18 C20,19.1045695 19.1045695,20 18,20 L6,20 C4.8954305,20 4,19.1045695 4,18 L4,12 C4,10.8954305 4.8954305,10 6,10 L7,10 Z M12,5 C10.3431458,5 9,6.34314575 9,8 L9,10 L15,10 L15,8 C15,6.34314575 13.6568542,5 12,5 Z" fill="#000000" />
                                  </g>
                                </svg>
                              </span>PR Manager</a>
                            <a href="#" className="text-muted text-hover-primary font-weight-bold">
                              <span className="svg-icon svg-icon-md svg-icon-gray-500 mr-1">
                                <svg xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                  <g stroke="none" strokeWidth={1} fill="none" fillRule="evenodd">
                                    <rect x={0} y={0} width={24} height={24} />
                                    <path d="M9.82829464,16.6565893 C7.02541569,15.7427556 5,13.1079084 5,10 C5,6.13400675 8.13400675,3 12,3 C15.8659932,3 19,6.13400675 19,10 C19,13.1079084 16.9745843,15.7427556 14.1717054,16.6565893 L12,21 L9.82829464,16.6565893 Z M12,12 C13.1045695,12 14,11.1045695 14,10 C14,8.8954305 13.1045695,8 12,8 C10.8954305,8 10,8.8954305 10,10 C10,11.1045695 10.8954305,12 12,12 Z" fill="#000000" />
                                  </g>
                                </svg>
                              </span>Bucharest</a>
                          </div>
                        </div>
                        <div className="my-lg-0 my-1">
                          <a href="#" className="btn btn-sm btn-light-primary font-weight-bolder text-uppercase mr-2">Ask</a>
                          <a href="#" className="btn btn-sm btn-primary font-weight-bolder text-uppercase">Hire</a>
                        </div>
                      </div>
                      <div className="d-flex align-items-center flex-wrap justify-content-between">
                      <div className="flex-grow-1 font-weight-bold text-dark-50 py-2 py-lg-2 mr-5">Experimentat in domeniu cu 10+ ani in PR si Marketing <br/> GMA si creativitate sporita . Match ridicat in cultura companiei.  </div>
                          <div className="d-flex mt-4 mt-sm-0">
                          <span className="font-weight-bold mr-4">Progress</span>
                          <div className="progress progress-xs mt-2 mb-2 flex-shrink-0 w-150px w-xl-250px">
                            <div className="progress-bar bg-warning" role="progressbar" style={{width: '83%'}} aria-valuenow={50} aria-valuemin={0} aria-valuemax={100} />
                          </div>
                          <span className="font-weight-bolder text-dark ml-4">83%</span>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="separator separator-solid my-7" />
                  <div className="d-flex align-items-center flex-wrap">
                    <div className="d-flex align-items-center flex-lg-fill mr-5 my-1">
                      <span className="mr-4">
                        <i className="flaticon-piggy-bank icon-2x text-muted font-weight-bold" />
                      </span>
                      <div className="d-flex flex-column text-dark-75">
                        <span className="font-weight-bolder font-size-sm">Raw Value</span>
                        <span className="font-weight-bolder font-size-h5">
                          <span className="text-dark-50 font-weight-bold">$</span>1100</span>
                      </div>
                    </div>
                    <div className="d-flex align-items-center flex-lg-fill mr-5 my-1">
                      <span className="mr-4">
                        <i className="flaticon-confetti icon-2x text-muted font-weight-bold" />
                      </span>
                      <div className="d-flex flex-column text-dark-75">
                        <span className="font-weight-bolder font-size-sm">GMA Score</span>
                        <span className="font-weight-bolder font-size-h5">
                          <span className="text-dark-50 font-weight-bold"></span>90,000</span>
                      </div>
                    </div>
                    <div className="d-flex align-items-center flex-lg-fill mr-5 my-1">
                      <span className="mr-4">
                        <i className="flaticon-pie-chart icon-2x text-muted font-weight-bold" />
                      </span>
                      <div className="d-flex flex-column text-dark-75">
                        <span className="font-weight-bolder font-size-sm">Culture Match</span>
                        <span className="font-weight-bolder font-size-h5">
                          <span className="text-dark-50 font-weight-bold"></span>90%</span>
                      </div>
                    </div>
                    <div className="d-flex align-items-center flex-lg-fill mr-5 my-1">
                      <span className="mr-4">
                        <i className="flaticon-file-2 icon-2x text-muted font-weight-bold" />
                      </span>
                      <div className="d-flex flex-column flex-lg-fill">
                        <span className="text-dark-75 font-weight-bolder font-size-sm">5 Jobs</span>
                        <a href="#" className="text-primary font-weight-bolder">View</a>
                      </div>
                    </div>
                    <div className="d-flex align-items-center flex-lg-fill mr-5 my-1">
                      <span className="mr-4">
                        <i className="flaticon-chat-1 icon-2x text-muted font-weight-bold" />
                      </span>
                      <div className="d-flex flex-column">
                        <span className="text-dark-75 font-weight-bolder font-size-sm">1 Recommandation</span>
                        <a href="#" className="text-primary font-weight-bolder">View</a>
                      </div>
                    </div>
                    
                  </div>
                </div>
              </div>
              <div className="card card-custom gutter-b">
                <div className="card-body">
                  <div className="d-flex">
                    <div className="flex-shrink-0 mr-7">
                      <div className="symbol symbol-50 symbol-lg-120">
                        <img alt="Pic" src="assets/media//users/300_10.jpg" />
                      </div>
                    </div>
                    <div className="flex-grow-1">
                      <div className="d-flex align-items-center justify-content-between flex-wrap mt-2">
                        <div className="mr-3">
                          <a href="#" className="d-flex align-items-center text-dark text-hover-primary font-size-h5 font-weight-bold mr-3">Andrei Vanea
                            <i className="flaticon2-correct text-success icon-md ml-2" /></a>
                          <div className="d-flex flex-wrap my-2">
                            <a href="#" className="text-muted text-hover-primary font-weight-bold mr-lg-8 mr-5 mb-lg-0 mb-2">
                              <span className="svg-icon svg-icon-md svg-icon-gray-500 mr-1">
                                <svg xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                  <g stroke="none" strokeWidth={1} fill="none" fillRule="evenodd">
                                    <rect x={0} y={0} width={24} height={24} />
                                    <path d="M21,12.0829584 C20.6747915,12.0283988 20.3407122,12 20,12 C16.6862915,12 14,14.6862915 14,18 C14,18.3407122 14.0283988,18.6747915 14.0829584,19 L5,19 C3.8954305,19 3,18.1045695 3,17 L3,8 C3,6.8954305 3.8954305,6 5,6 L19,6 C20.1045695,6 21,6.8954305 21,8 L21,12.0829584 Z M18.1444251,7.83964668 L12,11.1481833 L5.85557487,7.83964668 C5.4908718,7.6432681 5.03602525,7.77972206 4.83964668,8.14442513 C4.6432681,8.5091282 4.77972206,8.96397475 5.14442513,9.16035332 L11.6444251,12.6603533 C11.8664074,12.7798822 12.1335926,12.7798822 12.3555749,12.6603533 L18.8555749,9.16035332 C19.2202779,8.96397475 19.3567319,8.5091282 19.1603533,8.14442513 C18.9639747,7.77972206 18.5091282,7.6432681 18.1444251,7.83964668 Z" fill="#000000" />
                                    <circle fill="#000000" opacity="0.3" cx="19.5" cy="17.5" r="2.5" />
                                  </g>
                                </svg>
                              </span>andrei@vanea.com</a>
                            <a href="#" className="text-muted text-hover-primary font-weight-bold mr-lg-8 mr-5 mb-lg-0 mb-2">
                              <span className="svg-icon svg-icon-md svg-icon-gray-500 mr-1">
                                <svg xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                  <g stroke="none" strokeWidth={1} fill="none" fillRule="evenodd">
                                    <mask fill="white">
                                      <use xlinkHref="#path-1" />
                                    </mask>
                                    <g />
                                    <path d="M7,10 L7,8 C7,5.23857625 9.23857625,3 12,3 C14.7614237,3 17,5.23857625 17,8 L17,10 L18,10 C19.1045695,10 20,10.8954305 20,12 L20,18 C20,19.1045695 19.1045695,20 18,20 L6,20 C4.8954305,20 4,19.1045695 4,18 L4,12 C4,10.8954305 4.8954305,10 6,10 L7,10 Z M12,5 C10.3431458,5 9,6.34314575 9,8 L9,10 L15,10 L15,8 C15,6.34314575 13.6568542,5 12,5 Z" fill="#000000" />
                                  </g>
                                </svg>
                              </span>PR Manager</a>
                            <a href="#" className="text-muted text-hover-primary font-weight-bold">
                              <span className="svg-icon svg-icon-md svg-icon-gray-500 mr-1">
                                <svg xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                  <g stroke="none" strokeWidth={1} fill="none" fillRule="evenodd">
                                    <rect x={0} y={0} width={24} height={24} />
                                    <path d="M9.82829464,16.6565893 C7.02541569,15.7427556 5,13.1079084 5,10 C5,6.13400675 8.13400675,3 12,3 C15.8659932,3 19,6.13400675 19,10 C19,13.1079084 16.9745843,15.7427556 14.1717054,16.6565893 L12,21 L9.82829464,16.6565893 Z M12,12 C13.1045695,12 14,11.1045695 14,10 C14,8.8954305 13.1045695,8 12,8 C10.8954305,8 10,8.8954305 10,10 C10,11.1045695 10.8954305,12 12,12 Z" fill="#000000" />
                                  </g>
                                </svg>
                              </span>Charlie Stone</a>
                          </div>
                        </div>
                        <div className="my-lg-0 my-1">
                          <a href="#" className="btn btn-sm btn-light-primary font-weight-bolder text-uppercase mr-2">Ask</a>
                          <a href="#" className="btn btn-sm btn-primary font-weight-bolder text-uppercase">Hire</a>
                        </div>
                      </div>
                      <div className="d-flex align-items-center flex-wrap justify-content-between">
                        <div className="flex-grow-1 font-weight-bold text-dark-50 py-2 py-lg-2 mr-5">I distinguish three main text objectives could be merely to inform people.
                          <br />A second could be persuade people. You want people to bay objective.</div>
                        <div className="d-flex mt-4 mt-sm-0">
                          <span className="font-weight-bold mr-4">Progress</span>
                          <div className="progress progress-xs mt-2 mb-2 flex-shrink-0 w-150px w-xl-250px">
                            <div className="progress-bar bg-warning" role="progressbar" style={{width: '76%'}} aria-valuenow={50} aria-valuemin={0} aria-valuemax={100} />
                          </div>
                          <span className="font-weight-bolder text-dark ml-4">76%</span>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="separator separator-solid my-7" />
                  <div className="d-flex align-items-center flex-wrap">
                    <div className="d-flex align-items-center flex-lg-fill mr-5 my-1">
                      <span className="mr-4">
                        <i className="flaticon-piggy-bank icon-2x text-muted font-weight-bold" />
                      </span>
                      <div className="d-flex flex-column text-dark-75">
                        <span className="font-weight-bolder font-size-sm">Earnings</span>
                        <span className="font-weight-bolder font-size-h5">
                          <span className="text-dark-50 font-weight-bold">$</span>542,500</span>
                      </div>
                    </div>
                    <div className="d-flex align-items-center flex-lg-fill mr-5 my-1">
                      <span className="mr-4">
                        <i className="flaticon-confetti icon-2x text-muted font-weight-bold" />
                      </span>
                      <div className="d-flex flex-column text-dark-75">
                        <span className="font-weight-bolder font-size-sm">Expenses</span>
                        <span className="font-weight-bolder font-size-h5">
                          <span className="text-dark-50 font-weight-bold">$</span>675,500</span>
                      </div>
                    </div>
                    <div className="d-flex align-items-center flex-lg-fill mr-5 my-1">
                      <span className="mr-4">
                        <i className="flaticon-pie-chart icon-2x text-muted font-weight-bold" />
                      </span>
                      <div className="d-flex flex-column text-dark-75">
                        <span className="font-weight-bolder font-size-sm">Net</span>
                        <span className="font-weight-bolder font-size-h5">
                          <span className="text-dark-50 font-weight-bold">$</span>412,400</span>
                      </div>
                    </div>
                    <div className="d-flex align-items-center flex-lg-fill mr-5 my-1">
                      <span className="mr-4">
                        <i className="flaticon-file-2 icon-2x text-muted font-weight-bold" />
                      </span>
                      <div className="d-flex flex-column flex-lg-fill">
                        <span className="text-dark-75 font-weight-bolder font-size-sm">35 Tasks</span>
                        <a href="#" className="text-primary font-weight-bolder">View</a>
                      </div>
                    </div>
                    <div className="d-flex align-items-center flex-lg-fill mr-5 my-1">
                      <span className="mr-4">
                        <i className="flaticon-chat-1 icon-2x text-muted font-weight-bold" />
                      </span>
                      <div className="d-flex flex-column">
                        <span className="text-dark-75 font-weight-bolder font-size-sm">598 Comments</span>
                        <a href="#" className="text-primary font-weight-bolder">View</a>
                      </div>
                    </div>
                    <div className="d-flex align-items-center flex-lg-fill my-1">
                      <span className="mr-4">
                        <i className="flaticon-network icon-2x text-muted font-weight-bold" />
                      </span>
                      <div className="symbol-group symbol-hover">
                        <div className="symbol symbol-30 symbol-circle" data-toggle="tooltip" title="Mark Stone">
                          <img alt="Pic" src="assets/media/users/300_25.jpg" />
                        </div>
                        <div className="symbol symbol-30 symbol-circle" data-toggle="tooltip" title="Charlie Stone">
                          <img alt="Pic" src="assets/media/users/300_19.jpg" />
                        </div>
                        <div className="symbol symbol-30 symbol-circle" data-toggle="tooltip" title="Luca Doncic">
                          <img alt="Pic" src="assets/media/users/300_22.jpg" />
                        </div>
                        <div className="symbol symbol-30 symbol-circle" data-toggle="tooltip" title="Nick Mana">
                          <img alt="Pic" src="assets/media/users/300_23.jpg" />
                        </div>
                        <div className="symbol symbol-30 symbol-circle" data-toggle="tooltip" title="Teresa Fox">
                          <img alt="Pic" src="assets/media/users/300_18.jpg" />
                        </div>
                        <div className="symbol symbol-30 symbol-circle symbol-light" data-toggle="tooltip" title="More users">
                          <span className="symbol-label font-weight-bold">5+</span>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="card card-custom gutter-b">
                <div className="card-body">
                  <div className="d-flex">
                    <div className="flex-shrink-0 mr-7">
                      <div className="symbol symbol-50 symbol-lg-120 symbol-light-primary">
                        <span className="font-size-h3 symbol-label font-weight-boldest">SF</span>
                      </div>
                    </div>
                    <div className="flex-grow-1">
                      <div className="d-flex align-items-center justify-content-between flex-wrap mt-2">
                        <div className="mr-3">
                          <a href="#" className="d-flex align-items-center text-dark text-hover-primary font-size-h5 font-weight-bold mr-3">Sergei Ford
                            <i className="flaticon2-correct text-success icon-md ml-2" /></a>
                          <div className="d-flex flex-wrap my-2">
                            <a href="#" className="text-muted text-hover-primary font-weight-bold mr-lg-8 mr-5 mb-lg-0 mb-2">
                              <span className="svg-icon svg-icon-md svg-icon-gray-500 mr-1">
                                <svg xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                  <g stroke="none" strokeWidth={1} fill="none" fillRule="evenodd">
                                    <rect x={0} y={0} width={24} height={24} />
                                    <path d="M21,12.0829584 C20.6747915,12.0283988 20.3407122,12 20,12 C16.6862915,12 14,14.6862915 14,18 C14,18.3407122 14.0283988,18.6747915 14.0829584,19 L5,19 C3.8954305,19 3,18.1045695 3,17 L3,8 C3,6.8954305 3.8954305,6 5,6 L19,6 C20.1045695,6 21,6.8954305 21,8 L21,12.0829584 Z M18.1444251,7.83964668 L12,11.1481833 L5.85557487,7.83964668 C5.4908718,7.6432681 5.03602525,7.77972206 4.83964668,8.14442513 C4.6432681,8.5091282 4.77972206,8.96397475 5.14442513,9.16035332 L11.6444251,12.6603533 C11.8664074,12.7798822 12.1335926,12.7798822 12.3555749,12.6603533 L18.8555749,9.16035332 C19.2202779,8.96397475 19.3567319,8.5091282 19.1603533,8.14442513 C18.9639747,7.77972206 18.5091282,7.6432681 18.1444251,7.83964668 Z" fill="#000000" />
                                    <circle fill="#000000" opacity="0.3" cx="19.5" cy="17.5" r="2.5" />
                                  </g>
                                </svg>
                              </span>sergei@ford.com</a>
                            <a href="#" className="text-muted text-hover-primary font-weight-bold mr-lg-8 mr-5 mb-lg-0 mb-2">
                              <span className="svg-icon svg-icon-md svg-icon-gray-500 mr-1">
                                <svg xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                  <g stroke="none" strokeWidth={1} fill="none" fillRule="evenodd">
                                    <mask fill="white">
                                      <use xlinkHref="#path-1" />
                                    </mask>
                                    <g />
                                    <path d="M7,10 L7,8 C7,5.23857625 9.23857625,3 12,3 C14.7614237,3 17,5.23857625 17,8 L17,10 L18,10 C19.1045695,10 20,10.8954305 20,12 L20,18 C20,19.1045695 19.1045695,20 18,20 L6,20 C4.8954305,20 4,19.1045695 4,18 L4,12 C4,10.8954305 4.8954305,10 6,10 L7,10 Z M12,5 C10.3431458,5 9,6.34314575 9,8 L9,10 L15,10 L15,8 C15,6.34314575 13.6568542,5 12,5 Z" fill="#000000" />
                                  </g>
                                </svg>
                              </span>Angular Developer</a>
                            <a href="#" className="text-muted text-hover-primary font-weight-bold">
                              <span className="svg-icon svg-icon-md svg-icon-gray-500 mr-1">
                                <svg xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                  <g stroke="none" strokeWidth={1} fill="none" fillRule="evenodd">
                                    <rect x={0} y={0} width={24} height={24} />
                                    <path d="M9.82829464,16.6565893 C7.02541569,15.7427556 5,13.1079084 5,10 C5,6.13400675 8.13400675,3 12,3 C15.8659932,3 19,6.13400675 19,10 C19,13.1079084 16.9745843,15.7427556 14.1717054,16.6565893 L12,21 L9.82829464,16.6565893 Z M12,12 C13.1045695,12 14,11.1045695 14,10 C14,8.8954305 13.1045695,8 12,8 C10.8954305,8 10,8.8954305 10,10 C10,11.1045695 10.8954305,12 12,12 Z" fill="#000000" />
                                  </g>
                                </svg>
                              </span>Germany</a>
                          </div>
                        </div>
                        <div className="my-lg-0 my-1">
                          <a href="#" className="btn btn-sm btn-light-primary font-weight-bolder text-uppercase mr-2">Ask</a>
                          <a href="#" className="btn btn-sm btn-primary font-weight-bolder text-uppercase">Hire</a>
                        </div>
                      </div>
                      <div className="d-flex align-items-center flex-wrap justify-content-between">
                        <div className="flex-grow-1 font-weight-bold text-dark-50 py-2 py-lg-2 mr-5">I distinguish three main text objectives could be merely to inform people.
                          <br />A second could be persuade people.You want people to bay objective</div>
                        <div className="d-flex mt-4 mt-sm-0">
                          <span className="font-weight-bold mr-4">Progress</span>
                          <div className="progress progress-xs mt-2 mb-2 flex-shrink-0 w-150px w-xl-250px">
                            <div className="progress-bar bg-primary" role="progressbar" style={{width: '46%'}} aria-valuenow={50} aria-valuemin={0} aria-valuemax={100} />
                          </div>
                          <span className="font-weight-bolder text-dark ml-4">46%</span>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="separator separator-solid my-7" />
                  <div className="d-flex align-items-center flex-wrap">
                    <div className="d-flex align-items-center flex-lg-fill mr-5 my-1">
                      <span className="mr-4">
                        <i className="flaticon-piggy-bank icon-2x text-muted font-weight-bold" />
                      </span>
                      <div className="d-flex flex-column text-dark-75">
                        <span className="font-weight-bolder font-size-sm">Earnings</span>
                        <span className="font-weight-bolder font-size-h5">
                          <span className="text-dark-50 font-weight-bold">$</span>349,900</span>
                      </div>
                    </div>
                    <div className="d-flex align-items-center flex-lg-fill mr-5 my-1">
                      <span className="mr-4">
                        <i className="flaticon-confetti icon-2x text-muted font-weight-bold" />
                      </span>
                      <div className="d-flex flex-column text-dark-75">
                        <span className="font-weight-bolder font-size-sm">Expenses</span>
                        <span className="font-weight-bolder font-size-h5">
                          <span className="text-dark-50 font-weight-bold">$</span>654,200</span>
                      </div>
                    </div>
                    <div className="d-flex align-items-center flex-lg-fill mr-5 my-1">
                      <span className="mr-4">
                        <i className="flaticon-pie-chart icon-2x text-muted font-weight-bold" />
                      </span>
                      <div className="d-flex flex-column text-dark-75">
                        <span className="font-weight-bolder font-size-sm">Net</span>
                        <span className="font-weight-bolder font-size-h5">
                          <span className="text-dark-50 font-weight-bold">$</span>876,323</span>
                      </div>
                    </div>
                    <div className="d-flex align-items-center flex-lg-fill mr-5 my-1">
                      <span className="mr-4">
                        <i className="flaticon-file-2 icon-2x text-muted font-weight-bold" />
                      </span>
                      <div className="d-flex flex-column flex-lg-fill">
                        <span className="text-dark-75 font-weight-bolder font-size-sm">54 Tasks</span>
                        <a href="#" className="text-primary font-weight-bolder">View</a>
                      </div>
                    </div>
                    <div className="d-flex align-items-center flex-lg-fill mr-5 my-1">
                      <span className="mr-4">
                        <i className="flaticon-chat-1 icon-2x text-muted font-weight-bold" />
                      </span>
                      <div className="d-flex flex-column">
                        <span className="text-dark-75 font-weight-bolder font-size-sm">583 Comments</span>
                        <a href="#" className="text-primary font-weight-bolder">View</a>
                      </div>
                    </div>
                    <div className="d-flex align-items-center flex-lg-fill my-1">
                      <span className="mr-4">
                        <i className="flaticon-network icon-2x text-muted font-weight-bold" />
                      </span>
                      <div className="symbol-group symbol-hover">
                        <div className="symbol symbol-30 symbol-circle" data-toggle="tooltip" title="Mark Stone">
                          <img alt="Pic" src="assets/media/users/300_25.jpg" />
                        </div>
                        <div className="symbol symbol-30 symbol-circle" data-toggle="tooltip" title="Charlie Stone">
                          <img alt="Pic" src="assets/media/users/300_19.jpg" />
                        </div>
                        <div className="symbol symbol-30 symbol-circle" data-toggle="tooltip" title="Luca Doncic">
                          <img alt="Pic" src="assets/media/users/300_22.jpg" />
                        </div>
                        <div className="symbol symbol-30 symbol-circle" data-toggle="tooltip" title="Nick Mana">
                          <img alt="Pic" src="assets/media/users/300_23.jpg" />
                        </div>
                        <div className="symbol symbol-30 symbol-circle" data-toggle="tooltip" title="Teresa Fox">
                          <img alt="Pic" src="assets/media/users/300_18.jpg" />
                        </div>
                        <div className="symbol symbol-30 symbol-circle symbol-light" data-toggle="tooltip" title="More users">
                          <span className="symbol-label font-weight-bold">5+</span>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="card card-custom gutter-b">
                <div className="card-body">
                  <div className="d-flex">
                    <div className="flex-shrink-0 mr-7">
                    <div className="symbol symbol-50 symbol-lg-120 symbol-light-warning">
                        <span className="font-size-h3 symbol-label font-weight-boldest">IP</span>
                      </div>
                    </div>
                    <div className="flex-grow-1">
                      <div className="d-flex align-items-center justify-content-between flex-wrap mt-2">
                        <div className="mr-3">
                          <a href="#" className="d-flex align-items-center text-dark text-hover-primary font-size-h5 font-weight-bold mr-3">Ion Popescu
                            <i className="flaticon2-correct text-success icon-md ml-2" /></a>
                          <div className="d-flex flex-wrap my-2">
                            <a href="#" className="text-muted text-hover-primary font-weight-bold mr-lg-8 mr-5 mb-lg-0 mb-2">
                              <span className="svg-icon svg-icon-md svg-icon-gray-500 mr-1">
                                <svg xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                  <g stroke="none" strokeWidth={1} fill="none" fillRule="evenodd">
                                    <rect x={0} y={0} width={24} height={24} />
                                    <path d="M21,12.0829584 C20.6747915,12.0283988 20.3407122,12 20,12 C16.6862915,12 14,14.6862915 14,18 C14,18.3407122 14.0283988,18.6747915 14.0829584,19 L5,19 C3.8954305,19 3,18.1045695 3,17 L3,8 C3,6.8954305 3.8954305,6 5,6 L19,6 C20.1045695,6 21,6.8954305 21,8 L21,12.0829584 Z M18.1444251,7.83964668 L12,11.1481833 L5.85557487,7.83964668 C5.4908718,7.6432681 5.03602525,7.77972206 4.83964668,8.14442513 C4.6432681,8.5091282 4.77972206,8.96397475 5.14442513,9.16035332 L11.6444251,12.6603533 C11.8664074,12.7798822 12.1335926,12.7798822 12.3555749,12.6603533 L18.8555749,9.16035332 C19.2202779,8.96397475 19.3567319,8.5091282 19.1603533,8.14442513 C18.9639747,7.77972206 18.5091282,7.6432681 18.1444251,7.83964668 Z" fill="#000000" />
                                    <circle fill="#000000" opacity="0.3" cx="19.5" cy="17.5" r="2.5" />
                                  </g>
                                </svg>
                              </span>jason@siastudio.com</a>
                            <a href="#" className="text-muted text-hover-primary font-weight-bold mr-lg-8 mr-5 mb-lg-0 mb-2">
                              <span className="svg-icon svg-icon-md svg-icon-gray-500 mr-1">
                                <svg xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                  <g stroke="none" strokeWidth={1} fill="none" fillRule="evenodd">
                                    <mask fill="white">
                                      <use xlinkHref="#path-1" />
                                    </mask>
                                    <g />
                                    <path d="M7,10 L7,8 C7,5.23857625 9.23857625,3 12,3 C14.7614237,3 17,5.23857625 17,8 L17,10 L18,10 C19.1045695,10 20,10.8954305 20,12 L20,18 C20,19.1045695 19.1045695,20 18,20 L6,20 C4.8954305,20 4,19.1045695 4,18 L4,12 C4,10.8954305 4.8954305,10 6,10 L7,10 Z M12,5 C10.3431458,5 9,6.34314575 9,8 L9,10 L15,10 L15,8 C15,6.34314575 13.6568542,5 12,5 Z" fill="#000000" />
                                  </g>
                                </svg>
                              </span>PR Manager</a>
                            <a href="#" className="text-muted text-hover-primary font-weight-bold">
                              <span className="svg-icon svg-icon-md svg-icon-gray-500 mr-1">
                                <svg xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                  <g stroke="none" strokeWidth={1} fill="none" fillRule="evenodd">
                                    <rect x={0} y={0} width={24} height={24} />
                                    <path d="M9.82829464,16.6565893 C7.02541569,15.7427556 5,13.1079084 5,10 C5,6.13400675 8.13400675,3 12,3 C15.8659932,3 19,6.13400675 19,10 C19,13.1079084 16.9745843,15.7427556 14.1717054,16.6565893 L12,21 L9.82829464,16.6565893 Z M12,12 C13.1045695,12 14,11.1045695 14,10 C14,8.8954305 13.1045695,8 12,8 C10.8954305,8 10,8.8954305 10,10 C10,11.1045695 10.8954305,12 12,12 Z" fill="#000000" />
                                  </g>
                                </svg>
                              </span>Melbourne</a>
                          </div>
                        </div>
                        <div className="my-lg-0 my-1">
                          <a href="#" className="btn btn-sm btn-light-primary font-weight-bolder text-uppercase mr-2">Ask</a>
                          <a href="#" className="btn btn-sm btn-primary font-weight-bolder text-uppercase">Hire</a>
                        </div>
                      </div>
                      <div className="d-flex align-items-center flex-wrap justify-content-between">
                        <div className="flex-grow-1 font-weight-bold text-dark-50 py-2 py-lg-2 mr-5">I distinguish three main text objectives could be merely to inform people.
                          <br />A second could be persuade people. You want people to bay objective.</div>
                        <div className="d-flex mt-4 mt-sm-0">
                          <span className="font-weight-bold mr-4">Progress</span>
                          <div className="progress progress-xs mt-2 mb-2 flex-shrink-0 w-150px w-xl-250px">
                            <div className="progress-bar bg-success" role="progressbar" style={{width: '63%'}} aria-valuenow={50} aria-valuemin={0} aria-valuemax={100} />
                          </div>
                          <span className="font-weight-bolder text-dark ml-4">78%</span>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="separator separator-solid my-7" />
                  <div className="d-flex align-items-center flex-wrap">
                    <div className="d-flex align-items-center flex-lg-fill mr-5 my-1">
                      <span className="mr-4">
                        <i className="flaticon-piggy-bank icon-2x text-muted font-weight-bold" />
                      </span>
                      <div className="d-flex flex-column text-dark-75">
                        <span className="font-weight-bolder font-size-sm">Earnings</span>
                        <span className="font-weight-bolder font-size-h5">
                          <span className="text-dark-50 font-weight-bold">$</span>249,500</span>
                      </div>
                    </div>
                    <div className="d-flex align-items-center flex-lg-fill mr-5 my-1">
                      <span className="mr-4">
                        <i className="flaticon-confetti icon-2x text-muted font-weight-bold" />
                      </span>
                      <div className="d-flex flex-column text-dark-75">
                        <span className="font-weight-bolder font-size-sm">Expenses</span>
                        <span className="font-weight-bolder font-size-h5">
                          <span className="text-dark-50 font-weight-bold">$</span>164,700</span>
                      </div>
                    </div>
                    <div className="d-flex align-items-center flex-lg-fill mr-5 my-1">
                      <span className="mr-4">
                        <i className="flaticon-pie-chart icon-2x text-muted font-weight-bold" />
                      </span>
                      <div className="d-flex flex-column text-dark-75">
                        <span className="font-weight-bolder font-size-sm">Net</span>
                        <span className="font-weight-bolder font-size-h5">
                          <span className="text-dark-50 font-weight-bold">$</span>782,300</span>
                      </div>
                    </div>
                    <div className="d-flex align-items-center flex-lg-fill mr-5 my-1">
                      <span className="mr-4">
                        <i className="flaticon-file-2 icon-2x text-muted font-weight-bold" />
                      </span>
                      <div className="d-flex flex-column flex-lg-fill">
                        <span className="text-dark-75 font-weight-bolder font-size-sm">73 Tasks</span>
                        <a href="#" className="text-primary font-weight-bolder">View</a>
                      </div>
                    </div>
                    <div className="d-flex align-items-center flex-lg-fill mr-5 my-1">
                      <span className="mr-4">
                        <i className="flaticon-chat-1 icon-2x text-muted font-weight-bold" />
                      </span>
                      <div className="d-flex flex-column">
                        <span className="text-dark-75 font-weight-bolder font-size-sm">648 Comments</span>
                        <a href="#" className="text-primary font-weight-bolder">View</a>
                      </div>
                    </div>
                    <div className="d-flex align-items-center flex-lg-fill my-1">
                      <span className="mr-4">
                        <i className="flaticon-network icon-2x text-muted font-weight-bold" />
                      </span>
                      <div className="symbol-group symbol-hover">
                        <div className="symbol symbol-30 symbol-circle" data-toggle="tooltip" title="Mark Stone">
                          <img alt="Pic" src="assets/media/users/300_25.jpg" />
                        </div>
                        <div className="symbol symbol-30 symbol-circle" data-toggle="tooltip" title="Charlie Stone">
                          <img alt="Pic" src="assets/media/users/300_19.jpg" />
                        </div>
                        <div className="symbol symbol-30 symbol-circle" data-toggle="tooltip" title="Luca Doncic">
                          <img alt="Pic" src="assets/media/users/300_22.jpg" />
                        </div>
                        <div className="symbol symbol-30 symbol-circle" data-toggle="tooltip" title="Nick Mana">
                          <img alt="Pic" src="assets/media/users/300_23.jpg" />
                        </div>
                        <div className="symbol symbol-30 symbol-circle" data-toggle="tooltip" title="Teresa Fox">
                          <img alt="Pic" src="assets/media/users/300_18.jpg" />
                        </div>
                        <div className="symbol symbol-30 symbol-circle symbol-light" data-toggle="tooltip" title="More users">
                          <span className="symbol-label font-weight-bold">5+</span>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="card card-custom">
                <div className="card-body py-7">
                  <div className="d-flex justify-content-between align-items-center flex-wrap">
                    <div className="d-flex flex-wrap mr-3">
                      <a href="#" className="btn btn-icon btn-sm btn-light-primary mr-2 my-1">
                        <i className="ki ki-bold-double-arrow-back icon-xs" />
                      </a>
                      <a href="#" className="btn btn-icon btn-sm btn-light-primary mr-2 my-1">
                        <i className="ki ki-bold-arrow-back icon-xs" />
                      </a>
                      <a href="#" className="btn btn-icon btn-sm border-0 btn-hover-primary mr-2 my-1">...</a>
                      <a href="#" className="btn btn-icon btn-sm border-0 btn-hover-primary mr-2 my-1">23</a>
                      <a href="#" className="btn btn-icon btn-sm border-0 btn-hover-primary active mr-2 my-1">24</a>
                      <a href="#" className="btn btn-icon btn-sm border-0 btn-hover-primary mr-2 my-1">25</a>
                      <a href="#" className="btn btn-icon btn-sm border-0 btn-hover-primary mr-2 my-1">26</a>
                      <a href="#" className="btn btn-icon btn-sm border-0 btn-hover-primary mr-2 my-1">27</a>
                      <a href="#" className="btn btn-icon btn-sm border-0 btn-hover-primary mr-2 my-1">28</a>
                      <a href="#" className="btn btn-icon btn-sm border-0 btn-hover-primary mr-2 my-1">...</a>
                      <a href="#" className="btn btn-icon btn-sm btn-light-primary mr-2 my-1">
                        <i className="ki ki-bold-arrow-next icon-xs" />
                      </a>
                      <a href="#" className="btn btn-icon btn-sm btn-light-primary mr-2 my-1">
                        <i className="ki ki-bold-double-arrow-next icon-xs" />
                      </a>
                    </div>
                    <div className="d-flex align-items-center">
                      <select className="form-control form-control-sm text-primary font-weight-bold mr-4 border-0 bg-light-primary" style={{width: '75px'}}>
                        <option value={10}>10</option>
                        <option value={20}>20</option>
                        <option value={30}>30</option>
                        <option value={50}>50</option>
                        <option value={100}>100</option>
                      </select>
                      <span className="text-muted">Displaying 10 of 230 records</span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </>);
    
}